%global built_tag_strip 1.20

Summary: OCI runtime written in C
Name: crun
Epoch: 101
Version: %{built_tag_strip}
URL: https://github.com/containers/%{name}
Source0: %{url}/releases/download/%{version}/%{name}-%{version}.tar.xz
Packager: Podman Debbuild Maintainers <https://github.com/orgs/containers/teams/podman-debbuild-maintainers>
License: GPL-2.0+
Release: 0%{?dist}
BuildRequires: autoconf
BuildRequires: automake
BuildRequires: go-md2man
BuildRequires: libtool
%if "%{_vendor}" == "debbuild"
BuildRequires: git
BuildRequires: libcap-dev
BuildRequires: libseccomp-dev
BuildRequires: libsystemd-dev
BuildRequires: libyajl-dev
BuildRequires: pkg-config
BuildRequires: python3-dev
Requires: criu
%endif
Provides: oci-runtime

%description
%{name} is a runtime for running OCI containers

%prep
%autosetup -Sgit %{name}-%{built_tag_strip}

%build
./autogen.sh
%configure --disable-silent-rules %{krun_opts}
%make_build

%install
%make_install
rm -rf %{buildroot}%{_prefix}/lib*

%files
%license COPYING
%{_bindir}/%{name}
%{_mandir}/man1/*

%changelog
